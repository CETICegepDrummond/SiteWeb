<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partenaire extends Model
{
    /**
     * Obtenir l'image associée avec l'article.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne Le lien avec le modèle Image
     */
    public function image()
    {
        return $this->hasOne('App\Image', 'id', 'image_id');
    }

}
