<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use Illuminate\Support\Facades\Cache;

class CalendrierController extends Controller
{
    /**
     * Renvoie le calendrier en json.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Cache::remember('events', 2880, function () {
            return Event::all();
        });

        return response()->json($events, 200);
    }
}
