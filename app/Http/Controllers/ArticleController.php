<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use Illuminate\Support\Facades\Cache;

class ArticleController extends Controller
{
    /**
     * Renvoie tous les articles en json.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Cache::remember('articles', 2880, function () {
            return Article::with('image', 'user')->orderBy('created_at', 'desc')->get();
        });

        return response()->json($articles, 200);
    }

    /**
     * Renvoie les articles reliés au LAN.
     *
     * @return \Illuminate\Http\Response
     */
    public function lan()
    {
        $articles_lan = Cache::remember('articles-lan', 2880, function () {
            return Article::with('image', 'user')->orderBy('created_at', 'desc')->where('lan', 1)->get();
        });
        
        return response()->json($articles_lan, 200);
    }
}
