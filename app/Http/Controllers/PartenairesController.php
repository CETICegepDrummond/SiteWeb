<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Partenaire;
use Illuminate\Support\Facades\Cache;

class PartenairesController extends Controller
{
    /**
     * Renvoie tout les partenaires en json.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $partenaires = Cache::remember('partenaires', 2880, function () {
            return Partenaire::with('image')->get();
        });

        return response()->json($partenaires, 200);
    }
}
