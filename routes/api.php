<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/calendar', 'CalendrierController@index');
Route::get('/articles', 'ArticleController@index');
Route::get('/articles/lan', 'ArticleController@lan');
Route::get('/partenaires', 'PartenairesController@index');
Route::post('/contact', 'ContactController@store');