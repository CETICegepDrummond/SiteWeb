import Home from './views/Home.vue';
import Calendrier from './views/Calendrier.vue';
import Articles from './views/Articles.vue';
import Article from './views/Article.vue';
import Lan from './views/Lan.vue'
import Contact from './views/Contact.vue';

function setTitle(title) {
  return (route) => {
    return title;
  };
};

const routes = [
  {
    path: '/',
    component: Home,
    name: 'home',
    meta: {
      title: setTitle('Le CETI'),
    },
  },
  {
    path: '/calendrier',
    component: Calendrier,
    name: 'calendrier',
    meta: {
      title: setTitle('CETI - Calendrier des évenements'),
    },
  },
  {
    path: '/contactus',
    component: Contact,
    name: 'contactus',
    meta: {
      title: setTitle('CETI - Contactez nous'),
    },
  },
  {
    path: '/lan',
    component: Lan,
    name: 'lan',
    meta: {
      title: setTitle('CETI - Lan PAR-TI'),
    },
  },
  {
    path: '/articles',
    component: Articles,
    name: 'articles',
    meta: {
      title: setTitle('CETI - Tous les articles'),
    },
  },
  {
    path: '/article/:slug',
    component: Article,
    name: 'article',
    meta: {
      title: setTitle('CETI - Article -'),
    },
  },
];

export {routes};
