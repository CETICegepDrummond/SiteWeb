
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import VueRouter from 'vue-router';
import '@fortawesome/fontawesome-free/css/all.css'
import Vuetify from 'vuetify';
import { routes } from './routes.js';
import App from './App.vue';


require('./bootstrap');

window.Vue = require('vue');
window.axios.defaults.baseURL = '/';

Vue.component('default-layout', require('./layouts/Default.vue'));


Vue.use(Vuetify, {
    iconfont: 'fa4'
});
Vue.use(VueRouter);
Vue.use(require('vue-moment'));

const router = new VueRouter({
    mode: 'history',
    routes: routes,
});


router.afterEach((to, from) => {
    Vue.nextTick(() => {
        document.title = typeof to.meta.title === 'function' ? to.meta.title(to) : defaultTitle;
    });
});

const app = new Vue({
    el: '#app',
    components: { App },
    template: '<App/>',
    router: router,
});
